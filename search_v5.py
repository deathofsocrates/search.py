#  Title:         search_v.0.5
#
#  Author:        Adam Stevens
#                 SISTA/CS
#                 University of Arizona
#
#  Requested by:  Jennifer Earl PhD
#                 Professor of Sociology
#                 University of Arizona

#------------------------------------------------------------------IMPORTS----------------------------------------------------------------------------------------------------------------------------------

import os  #used in walking directories
import re  #used in word search

#------------------------------------------------------------------OBJECTS----------------------------------------------------------------------------------------------------------------------------------

class fileObject:
    """
    Creates a fileObject that contains information from a file search.
    Takes a string and a dictionary.
    """
    def __init__(self, fileName, wordDict):
        self.fileName = fileName                                                                                                           #sets file name
        self.wordDict = wordDict                                                                                                           #sets words found
        self.numberName = formatName(fileName)                                                                                             #sets file id
        self.wordCount = sumWordCount(wordDict)                                                                                            #sets total of words found
        self.wordString = keyString(wordDict)                                                                                              #sets words found
        self.outputString = str(self.numberName) + "\t" + str(self.fileName) + "\t" + self.wordString + "\t" + str(self.wordCount) + "\n"  #sets format for output string

#------------------------------------------------------------------DEFINITIONS------------------------------------------------------------------------------------------------------------------------------

def dir_and_file_list():
    """
    Takes all directories and puts them in a list.
    Takes all files and puts them in a list.

    Returns two lists.
    """
    fileList = []                               #holds files
    dirList = []                                #holds directories
    homeList = os.listdir(".")                  #sets list of containing files and directories
    for name in homeList:                       #iterates through homeList
        if os.path.isdir(name):                 #if directory
            dirList.append(name)                #append to dirList
        else:
            fileList.append(name)               #append to fileList
    return dirList, fileList                    #returns dirList and fileList

def formatName(fileName):
    """
    Creates a numerical file name from file name and maps it to the file's name.
    Takes a string.

    Returns a string.
    """
    return fileName.split("_")[0]               #returns a shortend version of the passed fileName

def sumWordCount(wordDict):
    """
    Creates a total by summing values in a dictionary.
    Takes a dictionary.

    Returns an int.
    """
    return sum(wordDict.values())               #returns the number of words found in the file

def keyString(wordDict):
    """
    Creates a string of words that were keys in a dictionary.
    Takes a dictionary.

    Returns a string.
    """
    return " ".join(wordDict.keys())            #returns a string of all the different words found

def strip_html(fileString):
    """
    Takes a string and removes any text contained by <>.

    Returns a string.
    """
    return strippedString.sub('', fileString)   #returns a string stripped of html

def exportFileObjects():
    """
    Writes a fileObject's outputString if it's wordDict contains anything.

    Returns void.
    """
    mapDict = {}                                                        #catches ids from fileObjects
    mapList = []                                                        #list of ids caught from mapDict
    if htmlBoolean:                                                     #checks if html strip was chosen from menu
        writeName = str(os.getcwd()) + listType + "_stripped.txt"       #creates .txt file named after current working directory for html stripped
    else:
        writeName = str(os.getcwd()) + listType + ".txt"                #creates .txt file named after current working directory for html not stripped
    textOut = open(writeName, 'w')                                      #opens .txt file as writable
    for i in range(len(fileObjectList)):                                #iterates through fileObjectList
        mapDict[fileObjectList[i].numberName] = fileObjectList[i]       #maps file ids to cancel duplicates
    for key in mapDict:                                                 #iterates through mapDict
        mapList.append(key)                                             #appends int ids to mapList
    mapList = sort_string_numbers(mapList)                              #bucket sorts files for output
    textOut.write(os.getcwd() + "\n\n")                                 #prints text files path
    textOut.write(wordListString)                                       #prints word list being used
    for j in range(len(mapList)):                                       #iterates through mapList
        for k in range(len(fileObjectList)):                            #iterates through fileObjectList
            if fileObjectList[k].numberName == mapList[j]:              #checks if file's id matches id from mapList (from sort)
                if len(fileObjectList[k].wordDict) > 0:                 #checks if fileObject actually found words
                    textOut.write(fileObjectList[k].outputString)       #writes objects output string to file

def sort_string_numbers(numbers):
    '''
    Takes a list.  Sorts file names into buckets by length, then sorts each bucket individually.
    Concatenate all of the buckets together.
    
    Returns a list.
    '''
    buckets = {}                              #dictionary to hold each sized file name
    for num in numbers:                       #for each item in numbers
        length = len(num)                     #gets length of item in numbers
        if buckets.get(length) is None:       #if bucket does not exist
            buckets[length] = []              #create a list for bucket length key
        buckets[length].append(num)           #add item to list in it's proper bucket
    for bucket in buckets:                    #for each bucket
        buckets[bucket].sort()                #sorts each bucket    
    result = []                               #creates sorted list to return
    for bucket in sorted(buckets.keys()):     #for each bucket size
        result.extend(buckets[bucket])        #concatenates sorted buckets together
    return result                             #returns sorted list of file names

def readFile(name):
    """
    Reads from a file and searches for certain keywords.
    Creates a file object and puts that object in a list.
    Takes a string.

    Returns void.
    """
    wordDict = {}                                                      #holds words found and how many times found
    file = open(name)                                                  #holds opened file
    if htmlBoolean:                                                    #checks if html strip was chosen from menu
        readFile = strip_html(file.read())                             #reads file and strips anything between <>
    else:
        readFile = file.read()                                         #reads file
    for i in range(len(wordList)):                                     #iterates through wordList
        tempList = re.findall(wordList[i], readFile, re.IGNORECASE)    #list of words found
        for i in range(len(tempList)):                                 #iterates through tempList
            if not tempList[i].lower() in nixWords:                    #checks if words found is in filter (nixWords)
                if wordDict.has_key(tempList[i]) == False:             #checks if word is already in wordDict
                    wordDict[tempList[i]] = 1                          #adds word and makes it count 1
                else:
                    wordDict[tempList[i]] = wordDict[tempList[i]] + 1  #adjusts word count by 1
    temp = fileObject(name, wordDict)                                  #holds created fileObject
    fileObjectList.append(temp)                                        #appends fileObject to fileObjectList

def readDirectory():
    """
    Opens a directory then reads every file in directory.
    Searches all files in sub-directories as well.
    Appends found words to a list that passes to a fileObject.

    Returns void.
    """
    wordDict = {}                                                              #holds words found and how many times found
    os.chdir(os.path.join(os.path.join(root, rootList[i]), dirList[k]))        #changes directory to elements in dirList
    for (path, dirs, files) in os.walk(os.getcwd()):                           #walks all paths, directories, and files in current directory
        for file in [ f for f in files ]:                                      #iterates through list of listed files (all files found in os.walk)
            fileLook = open(os.path.join(path, file))                          #opens file to read
            if htmlBoolean:                                                    #checks if html strip was chosen from menu
                readingFile = strip_html(fileLook.read())                      #reads file and strips anything between <>
            else:
                readingFile = fileLook.read()                                  #reads file
            for m in range(len(wordList)):                                     #iterates through wordList
                tempList = re.findall(wordList[m], readingFile, re.IGNORECASE) #list of words found
                for l in range(len(tempList)):                                 #iterates through tempList
                    if not tempList[l].lower() in nixWords:                    #checks if words found is in filter (nixWords)
                        if wordDict.has_key(tempList[l]) == False:             #checks if word is already in wordDict
                            wordDict[tempList[l]] = 1                          #adds word and makes it count 1
                        else:
                            wordDict[tempList[l]] = wordDict[tempList[l]] + 1  #adjusts word count by 1
    temp = fileObject(dirList[k], wordDict)                                    #holds created fileObject
    fileObjectList.append(temp)                                                #appends fileObject to fileObjectList

#------------------------------------------------------------------VARIABLES--------------------------------------------------------------------------------------------------------------------------------

menu = True
input = False
projectSelection = False
wordSelection = False

#------------------------------------------------------------------wordlists--------------------------------------------------------------------------------------------------------------#

youthList = [str(r'\byouth\w*'), str(r'\bstudent\w*'), str(r'\bcolleg\w*'), str(r'\buniversity\w*'), str(r'\byoung\w*'),              #words to search for (root words ending in wildcards)
    str(r'\bkid\w*'), str(r'\bchild\w*'), str(r'\beducation\w*'), str(r'\bschool\w*')]

protestList = [str(r'\bprotest\w*'), str(r'\bstrik\w*'), str(r'\brally\w*'), str(r'\bralli\w*'), str(r'\bdemonstrat\w*')]             #words to search for (root words ending in wildcards)

nixWords = ["kidney", "childnodes", "childnode", "childmenus", "childmenu", "childicon", "childicons", "childitems", "childitem",     #words to filter
    "childproxyframeexist", "childframeheight", "childproxyframe", "childelement", "childnodelength", "childelements",
    "childwins", "childobj", "childlinks", "childassets", "childid", "childof", "childtoken", "childmenuicon",
    "child_element", "child_menu"]

#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

strippedString = re.compile(r'<.*?>')                                                                                                 #regex compile for html strip

#------------------------------------------------------------------MENU-------------------------------------------------------------------------------------------------------------------------------------

while not projectSelection:                                                                         #keeps menu open until project is selected
    project = raw_input("Please select a project: \n 1.) Coded Files \n 2.) Other\nSelection: ")    #gives project options
    if project == "1":                                                                              #if selection is 1
        codedFiles = True                                                                           #sets main project
        projectSelection = True                                                                     #closes project selection menu
    elif project == "2":                                                                            #if selection is 2
        codedFiles = False                                                                          #sets other project
        projectSelection = True                                                                     #closes project selection menu
        projectDirTag = raw_input("\nPlease enter searchable directory keyword: ")                  #sets what directories will be searched based on user input
    else:
        print "\nPlease select a valid project\n"                                                   #loops project menu until valid response input

#-----------------------------------------------------------adding a new wordlist--------------------------------------------------------------------------------------------------------#
#                                                                                                                                                                                        #
# add a new list to "wordlists" under  the "VARIABLES" section                                                                                                                           #
# NOTE: each word in the new list must be in this format str(r'\b\w*'), where the word goes between the \b and \w*                                                                       #
#    EX: governList = [str(r'\bmayor\w*'), str(r'\bpolice\w*')]                                                                                                                          #
#                                                                                                                                                                                        #
# add a new number selection to the words variable                                                                                                                                       #
#    EX: words = raw_input("\nPlease select word list: \n 1.) Youth \n 2.) Protest \n 3.) PoliticsSelection: ")                                                                          #
#                                                                                                                                                                                        #
# add a new elif statement to this code block below "if" and above "else" where words equals the number you added in step one                                                            #
#    EX: elif words == "3":                                                                                                                                                              #
#                                                                                                                                                                                        #
# add wordList = "new list name" in new elif statement                                                                                                                                   #
#    EX: elif words == "3":                                                                                                                                                              #
#            wordList = governList                                                                                                                                                       #
#                                                                                                                                                                                        #
# add listType = "_NAMEOFLIST" in the new elif statement under the new wordList. this is the list name that appears on the output text file                                              #
#    EX: elif words == "3":                                                                                                                                                              #
#            wordList = governList                                                                                                                                                       #
#            listType = "_govern"                                                                                                                                                        #
#                                                                                                                                                                                        #
# add wordListString = "WORDS IN NEW LIST" in the new elif statement under the new listType. this is the first line of each output file                                                  #
# the words in this string are the words you added in the new wordlist that you added                                                                                                    #
#    EX: elif words == "3":                                                                                                                                                              #
#            wordList = governList                                                                                                                                                       #
#            listType = "_govern"                                                                                                                                                        #
#            wordListString = "mayor*, police*\n\n"                                                                                                                                      #
#                                                                                                                                                                                        #
# add wordSelection = True  this lets the menu know that you are done selecting the list                                                                                                 #
#    EX: elif words == "3":                                                                                                                                                              #
#            wordList = governList                                                                                                                                                       #
#            listType = "_govern"                                                                                                                                                        #
#            wordListString = "mayor*, police*\n\n"                                                                                                                                      #
#            wordSelection = True                                                                                                                                                        #
#                                                                                                                                                                                        #
                                                                                                                                                                                         #
while not wordSelection:                                                                                             #keeps menu open until word list is selected                        #
    words = raw_input("\nPlease select word list: \n 1.) Youth \n 2.) Protest\nSelection: ")                         #gives word list options                                            #
    if words == "1":                                                                                                 #if selection is 1                                                  #
        wordList = youthList                                                                                         #sets wordList to selection 1                                       #
        listType = "_youth"                                                                                          #adds word list nametag to output file                              #
        wordListString = "youth*, student*, colleg*, university*, young*, kid*, child*, education*, school*\n\n"     #creates string that appears in wordlist file output                #
        wordSelection = True                                                                                         #closes word list selection menu                                    #
    elif words == "2":                                                                                               #if selection is 2                                                  #
        wordList = protestList                                                                                       #sets wordList to selection 2                                       #
        listType = "_protest"                                                                                        #adds word list nametag to output file                              #
        wordListString = "protest*, strik*, rally*, ralli*, demonstrat*\n\n"                                         #creates string that appears in wordlist file output                #
        wordSelection = True                                                                                         #closes word list selection menu                                    #
    else:                                                                                                                                                                                #
        print "\nPlease select a valid word list\n"                                                                  #loops project menu until valid response input                      #
                                                                                                                                                                                         #
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

while not input:                                                                 #keeps menu open until html strip is selected
    html = raw_input("\nWould you like to strip the html? (Y/N) (Q to quit): ")  #gives html strip options
    if html.lower() == 'y':                                                      #if selection is y
        htmlBoolean = True                                                       #sets html strip to true
        menu = False                                                             #closes menu and runs program
        input = True                                                             #closes html strip selection menu
    elif html.lower() == 'n':                                                    #if selection is n
        htmlBoolean = False                                                      #sets html strip to false
        menu = False                                                             #closes menu and runs program
        input = True                                                             #closes html strip selection menu
    elif html.lower() == 'q':                                                    #if selection is q
        input = True                                                             #closes html strip selection menu and ends program
    else:
        print "Please enter Y, N, or Q"                                          #loops html menu until valid response input

if codedFiles:
    dirTag = "Coded Files"                                                       #sets project specific dirTag
else:
    dirTag = projectDirTag                                                       #sets dirTag to user input

#------------------------------------------------------------------MAIN-------------------------------------------------------------------------------------------------------------------------------------

if not menu:                                            #runs main after menu items are chosen
    fileObjectList = []                                 #empty list to hold fileObjects
    root = os.path.dirname(os.path.realpath(__file__))  #sets location of .py file
    os.chdir(root)                                      #makes current working directory
    rootList = dir_and_file_list()[0]                   #sets list to navigate from directories in .py file location
    for i in range(len(rootList)):                      #iterates directories where .py file is located
        os.chdir(os.path.join(root, rootList[i]))       #sets current directory to current in rootList iteration
        if dirTag.lower() in str(os.getcwd()).lower():  #checks if dirTag is in current directory name
            dirAndFiles = dir_and_file_list()           #creates lists of dirs and files
            fileList = dirAndFiles[1]                   #creates list of files
            dirList = dirAndFiles[0]                    #creates list of directories
            for a in range(len(fileList)):              #iterates through all files in directory
                readFile(fileList[a])                   #calls readFile() on file
            for k in range(len(dirList)):               #iterates through all directories in root directory list
                readDirectory()                         #calls readDirectory on each directory
            os.chdir(os.path.join(root, rootList[i]))   #changes working directory to next in line
            exportFileObjects()                         #calls function to write text files
            fileObjectList = []                         #resets fileObjectList




